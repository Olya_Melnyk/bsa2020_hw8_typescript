export function fight(firstFighter, secondFighter) {
    if (firstFighter.health <= 0) {
        return secondFighter;
    }
    else if (secondFighter.health <= 0) {
        return firstFighter;
    }
    else {
        firstFighter.health -= getDamage(secondFighter, firstFighter);
        fight(secondFighter, firstFighter);
    }
}
export function getDamage(attacker, enemy) {
    // damage = hit - block
    // return damage 
    let damage = getHitPower(attacker) - getBlockPower(enemy);
    return damage;
}
export function getHitPower(fighter) {
    let criticalHitChance = Math.random() + 1;
    let power = fighter.attack * criticalHitChance;
    return power;
    // return hit power
}
export function getBlockPower(fighter) {
    let dodgeChance = Math.random() + 1;
    let power = fighter.defense * dodgeChance;
    return power;
    // return block power
}
