import { callApi } from '../helpers/apiHelper';
import { IFighterDetails } from '../modals/interfaces/IFighterDetails';
import { IFighter } from '../modals/interfaces/IFighter';

export async function getFighters():Promise<IFighter[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return <IFighter[]>apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id:string):Promise<IFighter[]|IFighterDetails> {
  try{ 
    const endpoint=`details/fighter/${id}.json`;
    const apiResult=await callApi(endpoint, "GET");
    return apiResult;
    }
  catch(error){
    throw error;
  }
}

