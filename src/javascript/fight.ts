import { IFighterDetails } from "./modals/interfaces/IFighterDetails";

export function fight(firstFighter:IFighterDetails, secondFighter:IFighterDetails):IFighterDetails {
   const firstHealth=firstFighter.health;
    const secondHealth=secondFighter.health;
    const result=makeHits(firstFighter,secondFighter);
    firstFighter.health=firstHealth;
    secondFighter.health=secondHealth;
 return result;
}

function makeHits(firstFighter:IFighterDetails, secondFighter:IFighterDetails):IFighterDetails
{
  if(firstFighter.health<=0){
      return secondFighter;
    }
    else if(secondFighter.health<=0) { 
      return firstFighter;
    }
    else{
      firstFighter.health-=getDamage(secondFighter,firstFighter);
      return makeHits(secondFighter,firstFighter);
    }
}

export function getDamage(attacker:IFighterDetails, enemy:IFighterDetails):number {
  const damage:number = getHitPower(attacker) - getBlockPower(enemy);
  if(damage>0) return damage;
  else return 0;
}

export function getHitPower(fighter:IFighterDetails):number {
  let criticalHitChance:number=Math.random()+1;
  let power:number = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter:IFighterDetails):number {
  let dodgeChance:number=Math.random()+1;
  let power:number = fighter.defense * dodgeChance;
  return power;
}
