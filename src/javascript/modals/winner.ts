import { IFighterDetails } from "./interfaces/IFighterDetails";
import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";

export  function showWinnerModal(fighter:IFighterDetails):void {
  const title = 'Winner';
  const bodyElement = createWinnerModal(fighter);
  showModal({ title, bodyElement }); 
}
function createWinnerModal(fighter:IFighterDetails):HTMLElement{
  const {name, source}=fighter;
  const winnerDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const imageElement=createElement({tagName:'img', className:'fighter-image',attributes:{src:source}});
  nameElement.innerText = name;

  winnerDetails.append(nameElement,imageElement);
  return winnerDetails;
}