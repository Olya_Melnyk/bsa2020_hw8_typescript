import { createElement, IAttributes } from '../helpers/domHelper';
import { showModal } from './modal';
import { IFighterDetails } from './interfaces/IFighterDetails';

export  function showFighterDetailsModal(fighter:IFighterDetails) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter:IFighterDetails) {
  const { name,attack,defense,health,source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const attackElement=createElement({tagName:'div',className:'attack'});
  const defenseElement=createElement({tagName:'div',className:'defense'});
  const healthElement=createElement({tagName:'div',className:'health'});
  const attr:IAttributes={src:source};
  const imageElement=createElement({tagName:'img', className:'fighter-image',attributes:attr})
  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  attackElement.innerText='attack:'+' '+attack.toString();
  defenseElement.innerText='defense:'+' '+defense.toString();
  healthElement.innerText='health:'+' '+health.toString();

  fighterDetails.append(nameElement,attackElement,defenseElement,healthElement,imageElement);


  return fighterDetails;
}
