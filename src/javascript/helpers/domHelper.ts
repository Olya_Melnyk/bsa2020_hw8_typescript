export interface IAttributes {
    [propName : string] : string;
}
interface IElement{
    tagName:string,
    className?:string,
    attributes?:IAttributes
}
export function createElement({ tagName, className, attributes = {} }:IElement):HTMLElement {
  const element = document.createElement(tagName);
  
  if (className) {
    element.classList.add(className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
}