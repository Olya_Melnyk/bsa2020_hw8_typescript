import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighterDetails } from './modals/interfaces/IFighterDetails';
import { IFighter } from './modals/interfaces/IFighter';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters:IFighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event:Event, fighter:IFighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId:string):Promise<IFighterDetails> {
    let fighterDetails:IFighterDetails;
    if (fightersDetailsCache.has(fighterId)) {
      fighterDetails  = fightersDetailsCache.get(fighterId);
    }
    else {
      fighterDetails= await getFighterDetails(fighterId) as IFighterDetails;
      fightersDetailsCache.set(fighterId, fighterDetails);
    }
    return new Promise((resolve, reject) => {
      setTimeout(() => (fighterDetails ? resolve(fighterDetails) : reject(Error('Failed to load'))), 500);
    }); 
}

function createFightersSelector() {
  const selectedFighters:Map<string, IFighterDetails> = new Map();

  return async function selectFighterForBattle(event:any, fighter:IFighter):Promise<void>{
    const fullInfo = await getFighterInfo(fighter._id) as IFighterDetails;
    if (event.target.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }
    if (selectedFighters.size === 2) {
      const fighters = [...selectedFighters];
      const winner = fight(fighters[0][1],fighters[1][1]);
      showWinnerModal(winner);
    }
  }
}
